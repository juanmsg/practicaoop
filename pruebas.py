import unittest
from coche import Coche  # Suponiendo que tienes la clase Coche en un archivo coche.py

class TestCoche(unittest.TestCase):

    def setUp(self):
        self.coche = Coche("Rojo", "Toyota", "Corolla", "1234ABC", 0)

    def test_acelerar(self):
        self.coche.acelerar()
        self.assertEqual(self.coche.velocidad, 10)

    def test_frenar(self):
        self.coche.frenar()
        self.assertEqual(self.coche.velocidad, -10)

if __name__ == '__main__':
    unittest.main()